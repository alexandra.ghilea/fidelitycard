# Create your views here.
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from .forms import MyFormLogin, UserProfileInfoForm, EditProfileForm, CardCreateForm
from .models import Card, UserProfileInfo, CardType, Shop
from django.views.generic import CreateView, ListView, DetailView, UpdateView, DeleteView

from .models import User
from django.contrib.auth.models import User
from django.contrib.auth.forms import PasswordChangeForm, UserChangeForm


def base_view(request):
    context = {
        'blog_entries': [
            {
                'title': 'Hello World',
                'body': 'I have created ...'
            },
            {'title': 'A title',
             'body': 'And a description'}
        ]
    }
    return render(request, 'base.html')


@login_required
def special(request):
    return HttpResponse("You are logged in !")


@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('home'))


def user_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request, user)
                # return HttpResponseRedirect(reverse('user_page'))
                return HttpResponseRedirect(reverse('pages:user_page'))
                # return HttpResponse(reverse('user_page'))
            else:
                return HttpResponse("Your account was inactive.")
        else:
            print("Someone tried to login and failed.")
            print("They used username: {} and password: {}".format(username, password))
            # return HttpResponse("Invalid login details given or you must create an account")
            return HttpResponseRedirect(reverse('pages:register'))

    else:
        return render(request, 'login.html', {})


def register(request):
    registered = False
    if request.method == 'POST':
        user_form = MyFormLogin(data=request.POST)
        profile_form = UserProfileInfoForm(data=request.POST)

        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            profile = profile_form.save(commit=False)
            profile.user = user
            if "profile_pic" in request.FILES:
                print("Found it")
                profile.profile_pic = request.FILES["profile_pic"]

            profile.save()
            registered = True
        else:
            print(user_form.errors, profile_form.errors)

    else:
        user_form = MyFormLogin()
        profile_form = UserProfileInfoForm()
    return render(request, 'registration.html',
                  {'user_form': user_form,
                   'profile_form': profile_form,
                   'registered': registered})


def home_page(request):
    return render(request, 'home.html')


def user_page(request):
    return render(request, 'user_page.html', context={'user': request.user})


def view_profile(request):
    args = {'user': request.user}
    return render(request, 'profile.html', args)


def view_profile_info(request):
    context = {}
    if request.user is not None and not request.user.is_anonymous:
        context['photo'] = UserProfileInfo.objects.get(user=request.user)

    return render(request, 'profile.html', context=context)


def edit_profile(request):
    if request.method == 'POST':
        print(request.POST)
        form = EditProfileForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            return redirect('pages:profile')
    else:
        form = EditProfileForm(instance=request.user)
        args = {'form': form}

        return render(request, 'profile_update.html', args)


def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(data=request.POST, user=request.user)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            return redirect('pages:profile')
    else:
        form = PasswordChangeForm(user=request.user)
        args = {'form': form}

        return render(request, 'change_password.html', args)


def display_all_cards(request):
    all_cards = Card.objects.all()
    return render(request, 'display_cards.html', context={'all_cards': all_cards})


def display_photo_user(request):
    context = {}
    if request.user is not None and not request.user.is_anonymous:
        context['photo'] = UserProfileInfo.objects.get(user=request.user)
        print(context['photo'].__dict__)
    return render(request, 'user_page.html', context=context)


class ProfileUpdateView(UpdateView):
    model = UserProfileInfo
    template_name = 'profile_update.html'
    context_object_name = 'profile_infos'
    fields = ['profile_pic']
    success_url = reverse_lazy('pages:user_page')


class CardListView(ListView):
    model = Card
    template_name = 'card_list.html'
    context_object_name = 'cards'

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context['cards'] = Card.objects.filter(client_id=self.request.user)
        print(context['cards'])
        return context


class CardCreateView(CreateView):
    model = Card
    template_name = 'card_add.html'
    form_class = CardCreateForm
    success_url = reverse_lazy('pages:user_page')

    def form_valid(self, form):
        form.instance.client_id = self.request.user
        return super().form_valid(form)


class CardDetailView(DetailView):
    model = Card
    template_name = 'card_detail.html'
    context_object_name = 'card'


class CardDeleteView(DeleteView):
    model = Card
    template_name = 'card_delete.html'
    context_object_name = 'cards'
    success_url = reverse_lazy('pages:user_page')


# class CardTypeView(ListView):
#     model=ListView
#     template_name = 'test.html'
#     context_object_name = 'card_type'
#     success_url = reverse_lazy('test')

def display_card_photo(request):
    context = {}
    context['card_photo'] = CardType.objects.get()
    return render(request, 'test.html', context=context)

    # context = {}
    # context['card_photo'] = CardType.objects.all()
    # return render(request, 'test.html', context=context)


def update_cards_for_shop(request):
    shop = request.GET.get('shop')
    found_shops = Shop.objects.filter(pk__exact=shop)
    cards = []
    for found_shop in found_shops:
        cards = found_shop.cardtype_set.all()
        break
    return render(request, 'add_card_of_shop.html', {'cards': cards})
