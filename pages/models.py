from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model


# Create your models here.
#
# class User(models.Model):
#     name = models.TextField(max_length=120)
#     surname = models.TextField(max_length=120)
#     email = models.EmailField()
#     phone = models.TextField()


class UserProfileInfo(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    profile_pic = models.ImageField(upload_to='static/profile_pics', blank=True)


    def __str__(self):
        return self.user.username


class Shop(models.Model):
    shop_name = models.TextField(max_length=50)

    def __str__(self):
        return self.shop_name


class CardType(models.Model):
    card_name = models.TextField(max_length=50)
    card_picture = models.ImageField(upload_to='static/profile_pics', blank=False)
    shop_id = models.ForeignKey(Shop, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.card_name


class Card(models.Model):
    shop_id = models.ForeignKey(Shop, on_delete=models.CASCADE, null=True)
    card_id = models.ForeignKey(CardType, on_delete=models.CASCADE, null=True)
    client_id = models.ForeignKey(User, on_delete=models.CASCADE)

# class CardsInfo(models.Model):
#     shop_id=models.CharField(max_length=50)
#     shop_name=models.TextField()
#     card_name=models.TextField()
#     card_image=models.ImageField(upload_to='card_pics',blank=True)
#
