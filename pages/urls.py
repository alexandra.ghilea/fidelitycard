from django.contrib import admin

from django.urls import path

from . import views

# from pages.views import base_view, login_page

# from pages.views import AboutView
from django.conf.urls import url

app_name = 'pages'

urlpatterns = [

    path('', views.home_page, name='home_view'),
    path('card_list/', views.CardListView.as_view(), name='card_list'),
    path('card_add/', views.CardCreateView.as_view(), name='card_add'),
    path('card_detail/<int:pk>', views.CardDetailView.as_view(), name='card_detail'),
    path('card_delete/<int:pk>', views.CardDeleteView.as_view(), name='card_delete'),
    path('user_page/', views.display_photo_user, name='user_page'),
    path('test/', views.display_card_photo, name='test'),
    # path('profile_update/',views.update_user, name='profile_update'),
    # path('profile_update/', views.ProfileUpdateView.as_view(), name='profile_update'),
    path('profile/',views.view_profile,name='profile'),
    path('profile/',views.view_profile_info, name='profile'),
    path('profile_update/',views.edit_profile, name='profile_update'),
    path('change_password/',views.change_password, name='change_password'),
    path('password/',views.change_password, name='password'),
    path('add_card_of_shop/', views.update_cards_for_shop, name='add_card_of_shop'),

    url('^user_page/$', views.user_page, name='user_page'),
    url('^display_cards/$', views.display_all_cards, name='display_cards'),

    url(r'^register/$', views.register, name='register'),
    url(r'^user_login/$', views.user_login, name='user_login'),

]
