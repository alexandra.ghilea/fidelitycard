from django.contrib import admin

# Register your models here.
from pages.models import User,UserProfileInfo,Card, CardType,Shop

# admin.site.register(User)
admin.site.register(UserProfileInfo)
admin.site.register(Card)
admin.site.register(CardType)
admin.site.register(Shop)