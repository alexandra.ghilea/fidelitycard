import re

from django import forms
from django.contrib.auth.forms import UserChangeForm
# from pages.models import UserProfileInfo
from django.contrib.auth.models import User

from pages.models import UserProfileInfo, Card, Shop, CardType


class MyFormLogin(forms.ModelForm):
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ("username", "password", "email", "last_name", "first_name")

    def clean_password(self, *args, **kwargs):
        password = self.cleaned_data.get("password")
        if len(password) < 10:
            raise forms.ValidationError("Length password should be at least 11")
        elif re.search('[0-9]', password) is None:
            raise forms.ValidationError("Your password must contain at least one digit")
        elif re.search('[A-Z]', password) is None:
            raise forms.ValidationError("Your password must contain a capital letter in it")
        return password

    def clean_first_name(self, *args, **kwargs):
        if self.cleaned_data["first_name"].strip() == '':
            raise forms.ValidationError("You must enter a value for First Name")
        return self.cleaned_data["first_name"]

    def clean_last_name(self, *args, **kwargs):
        if self.cleaned_data["last_name"].strip() == '':
            raise forms.ValidationError("You must enter a value for Last Name")
        return self.cleaned_data["last_name"]


class UserProfileInfoForm(forms.ModelForm):
    class Meta:
        model = UserProfileInfo
        fields = ("profile_pic",)
    # def clean_profile_pic(self):
    #     if self.clean_profile_pic["profile_pic"]


class EditProfileForm(UserChangeForm):
    class Meta:
        model = User
        fields = ("email", "last_name", "first_name")
    # def clean_first_name(self,*args,**kwargs):
    #     if self.cleaned_data["first_name"].strip() == '':
    #         raise forms.ValidationError("You must enter a value for First Name")
    #     return self.cleaned_data["first_name"]
    # def clean_last_name(self, *args, **kwargs):
    #     if self.cleaned_data["last_name"].strip() == '':
    #         raise forms.ValidationError("You must enter a value for Last Name")
    #     return self.cleaned_data["last_name"]


# class EditUserProfileInfoForm(UserProfileInfoForm):
#     class meta():
#         model=UserProfileInfo
#         field=("profile_pic")


class CardCreateForm(forms.ModelForm):
    class Meta:
        model = Card
        fields = ("shop_id","card_id")

    shop_id = forms.ModelChoiceField(required=True, widget=forms.Select, queryset=Shop.objects.all())

    def __init__(self, *args, **kwargs):
        super(CardCreateForm, self).__init__(*args, **kwargs)
        self.fields['card_id'] = forms.ModelChoiceField(required=True, widget=forms.Select,
                                                        queryset=CardType.objects.none())
        if 'shop_id' in self.data:
            shop_id = int(self.data.get('shop_id'))
            self.fields['card_id'].queryset = CardType.objects.filter(shop_id=shop_id)
        print(self.fields['shop_id'].widget.choices.__dict__)
        # elif self.instance.pk:
        #     self.fields['card_id'].queryset = self.instance.shop.city_set
